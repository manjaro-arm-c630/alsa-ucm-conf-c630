# Maintainer: David Runge <dvzrv@archlinux.org>
# Contributor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>

pkgname=alsa-ucm-conf
pkgver=1.2.4
pkgrel=1
pkgdesc="ALSA Use Case Manager configuration (and topologies)"
url="https://alsa-project.org/"
arch=('any')
license=('BSD')
source=("https://www.alsa-project.org/files/pub/lib/${pkgname}-${pkgver}.tar.bz2"{,.sig}
		# Patches for Lenovo Yoga C630 13Q50 sound support
		'0001-ucm2-DB845c-fixes-HDMI-select-card-and-HiFi-set-Digi.patch'
		'0001-sdm845-add-LENOVO-Yoga-C630-support.patch'
		'0003-ucm.conf-support-KernelModule-CardLongName.conf-path.patch'
		'0005-ucm2-codecs-lpass-add-codec-sequences-for-wsa-and-va.patch'
		'0006-ucm2-add-support-to-for-Qualcomm-RB5-Platform.patch'
		'0001-ucm2-codecs-wcd938x-update-enable-disable-sequence.patch'
		'0002-sdm845-add-LENOVO-Yoga-C630-support.patch'
)
sha512sums=('9043460e92b2ed44757b08b9faca888e8bfae40d84e4ad7e7df44df2bb3b0617e86ef23783973accd62fb6681788262e67212e2bf67178d75781e57a0fa346d2'
            'SKIP'
            '7011d32e5ec17edf6a3ed371abc90eb877f46d65787446af7009d62a713a8409713cdd345fdec496783e6d62846f0ec109a53f6facfbceb35e888d4b46ccf390'
            '5175d1e9dd3790ba479575628948eacfa4c8402fb0d7d34f3da9888cfeef6d974aea7138cddcaeb3a0fd8a07b73f4d6400b5cc783ed8670100293b4a9350e3d5'
            '6504a64007bc4f353b7c1240f84f204ad08a3d7043c8952c09c6ce658bfdaad1de3bdcd5c7ef67b15cedc79ba5727a4bb6f7b57ffa7f9686c2d592573fc570a5'
            'c702d35045ffb0747bee1fe4bbeffccf56aad6b1653efa43308204f449d4bdc755654d69ce2db9e34e59450073c49d03319dcccfa4ff29fc87e8c66fbf52356d'
            '71fe8184ecb6f5ef1580b76220ae25f5deca8cf02cd6a23f33c920ea109fc20ccd7f5f845a99bbd6c978d2e2b4eebfb5052a456ae2d1df3eca026e937505028c'
            '22d1a2edf65ee3b1fc8ae13ee7d6629a0b20c0f1868147f37d9df1db706f285c5cc3fdb1697e241d9ed3765997c63264dacd007999a590925454812837341686'
            '0ad6162f8d617ef877d08f38f4363785a93dbc21914fa58130b358c8a977e893a27a5413f1796b9d2918b526ff8b94a9173abc69e7a3e7a4d9d6ff11879da1b9'
            'a33a039bda7284993ffcc4751666311c4366f747ec49b5f0cf8f39fe970508b213d975d33830b2d451128ba0ad1af80a86c667eafaea292b1e8381cde81110b4'
            '2c90dc0558b6970e97744df4c38c7060f9e3db0e229bb190f47b2cc900bbc550fd390f6720dff820dca082918ef79cd9dac7ec33475d38059332f312e6124d62')
b2sums=('545f3bfb36c6c41e48a5d6c2a98b936b2e71d8aab99227faecfcb1dd1dec9a03eb25f1664ca9aad3f9371f140b548258bba5a3904f5a3ec83513eb72abe7c7c2'
        'SKIP'
        'bed9f844216e367da605fef27b6d817586ad17ed3c5fa0872d08073d08634e13a2bba253d23aaa0b416850ebc022eb700e4be539f597595fd592bf81f8ff0131'
        '394137b362c9535318d7eef4fee411fb6ab434910d2adb84cf33a7a83da4b45fa143cab1d593e0d2deb17954471696adb30d5a21ba0fa1141497035baef6dc67'
        '16dfe248d0bde33e7c6db7728301d40945470cdc8e463c53ec5e77fe15b7eecf3540194a9836bfbe33d3a658d869cb0f207e6f64c615c377bac09340b369fbdc'
        '25b8915fd5841d1a7b234a7ba01e40fd3bc238a7a719d31c3f08faf8e40cc1850ca8672e79f8201eef184c264ac0718bb7e5b51cad52b964be4a5476c1091228'
        '898d615f3a6a0c9d9c06c281d07927d2d56368a0a3d8737e871b04498b5d70b962cb19bd32986a91b4a7f03f4814a07dfa66de13b211bdd8c0890534b2736ebe'
        '83061d9c812c6eecef12a1d629e0163b13eda7940d67844b056f57abb195588c4449f7adfc18c2ef163ca01cfeebf07128eedaf3d3c372a64a052e2d32e66b57'
        'b2356a65f12dec10fdc1aa72d569c86662576eda5d939dbc34cabc7de14b2906fd8dc1e3cad6237d2e2be0756d816d12ff1614125871d7ca48874b6df801934c'
        '0e48ef681aee9aff0e02afe1a2c6f686cf338672ea01840d55e1e094d0cdd50966001d153ab0b8674c7167eee8982962e229377fbd7da9582399f507675ebfaa'
        'bf340146f795cefa06f37a9cb37cd59c5d038d339b05ce9382536b1e467059b4e6cfd8f159fa26f610d6c92ff231e7630b492f93a725004fee7484ef0e33f876')
validpgpkeys=('F04DF50737AC1A884C4B3D718380596DA6E59C91') # ALSA Release Team (Package Signing Key v1) <release@alsa-project.org>

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  
  # Apply patches
  patch --forward --strip=1 --input="${srcdir}/0001-ucm2-DB845c-fixes-HDMI-select-card-and-HiFi-set-Digi.patch"
  patch --forward --strip=1 --input="${srcdir}/0001-sdm845-add-LENOVO-Yoga-C630-support.patch"
  patch --forward --strip=1 --input="${srcdir}/0003-ucm.conf-support-KernelModule-CardLongName.conf-path.patch"
  patch --forward --strip=1 --input="${srcdir}/0005-ucm2-codecs-lpass-add-codec-sequences-for-wsa-and-va.patch"
  patch --forward --strip=1 --input="${srcdir}/0006-ucm2-add-support-to-for-Qualcomm-RB5-Platform.patch"
  patch --forward --strip=1 --input="${srcdir}/0001-ucm2-codecs-wcd938x-update-enable-disable-sequence.patch"
  patch --forward --strip=1 --input="${srcdir}/0002-sdm845-add-LENOVO-Yoga-C630-support.patch"
}

package() {
  cd "${pkgname}-${pkgver}"
  find ucm2 -type f -iname "*.conf" -exec install -vDm 644 {} "${pkgdir}/usr/share/alsa/"{} \;
  find ucm2 -type l -iname "*.conf" -exec cp -dv {} "${pkgdir}/usr/share/alsa/"{} \;
  install -vDm 644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
  install -vDm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
  install -vDm 644 ucm2/README.md -t "$pkgdir/usr/share/doc/$pkgname/ucm2"

  # Make some symbolic links
  cd "$pkgdir/usr/share/alsa/ucm2/module"
  ln -s "../Qualcomm/sdm845" "snd_soc_sdm845"
  ln -s "../Qualcomm/sm8250" "snd_soc_sm8250"
}
